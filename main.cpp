#include "TcpStream.h"
#include "SmartBuffer.h"
#include <iostream>
#include <thread>


vector<string> peerList;


void threadedRead(Winux::TcpSocket *peer)
{
  Network::TcpStream stream(peer);
  SmartBuffer buffer;
  stream >> buffer;
  
  if(buffer.size() == 0)
  {
    std::cout << peer->getIPAddress() << ":" << peer->getPort() << " joined." << std::endl;
    peerList.push_back(peer->getIPAddress());
  }
  else
  {
    std::cout << string(reinterpret_cast<char *>(buffer.data()), buffer.size()) << std::endl;
  }
  
  peer->close();
  delete peer;
}

void threadedWrite(string *ip, string *message)
{
  Winux::TcpSocket connection;
  connection.setup();
  connection.connect(*ip, 23041);
  
  Network::TcpStream stream(&connection);
  SmartBuffer buffer;
  buffer << *message;
  stream << buffer;
  
  connection.close();
}

void acceptCallback(Winux::TcpSocket *acceptor)
{
  try
  {
    while(true)
    {
      Winux::TcpSocket *peer = acceptor->accept();
      
      std::thread readThread(threadedRead, peer);
      readThread.detach();
    }
  }
  catch(NetcomException &e)
  {
    std::cout << "Error: " << e.getMessage() << " [Code: " << e.getError() << "]" << std::endl;
  }
}

int main(int argc, char **argv)
{
  Winux::TcpSocket acceptor;
  
  try
  {
    acceptor.setup();
    acceptor.reuseAddress(true);
    acceptor.bind(23041);
    acceptor.listen(50);
    
    std::thread acceptorThread(acceptCallback, &acceptor);
    acceptorThread.detach();
    
    string message;
    getline(std::cin, message);
    
    while(message != "exit")
    {
      for(auto ip : peerList)
      {
        std::thread writeThread(threadedWrite, &ip, &message);
        writeThread.join();
      }
      
      getline(std::cin, message);
    }
    
    if(acceptor.isOpen())
      acceptor.close();
  }
  catch(NetcomException &e)
  {
    std::cout << "Error: " << e.getMessage() << " [Code: " << e.getError() << "]" << std::endl;
  }
  
  return 0;
}
