#ifndef StreamException_H
#define StreamException_H

//
// Include
//
#include "NetcomException.h"


class StreamException : public NetcomException
{
public:
  /**
   * Overloaded constructor
   */
  StreamException(const string message, const int32_q error)
  {
    this->message = message;
    this->error = error;
  }
  
  /**
   * Default destructor
   */
  virtual ~StreamException() {}
};


#endif  // StreamException_H
