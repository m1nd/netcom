#ifndef Stream_H
#define Stream_H

//
// Includes
//
#include "Netcom.h"
#include "SmartBuffer.h"
#include "StreamException.h"


namespace Network
{
  class Stream
  {
  protected:
    /**
     * Frame buffer size
     */
    uint32_q frameBufferSize;
    
    /**
     * Written bytes
     */
    uint64_q writtenBytes;
    
    /**
     * Read bytes
     */
    uint64_q readBytes;
    
    
  public:
    /**
     * Default constructor
     */
    Stream() : frameBufferSize(1024), writtenBytes(0), readBytes(0) {}
    
    /**
     * Default destructor
     */
    virtual ~Stream() {}
    
    /**
     * Get the frame buffer size.
     *
     * @return  Frame buffer size
     */
    uint32_q getFrameBufferSize()
    {
      return frameBufferSize;
    }
    
    /**
     * Get the written bytes count.
     *
     * @return  Written bytes
     */
    uint64_q getWrittenBytes()
    {
      return writtenBytes;
    }
    
    /**
     * Get the read bytes count.
     *
     * @return  Read bytes
     */
    uint64_q getReadBytes()
    {
      return readBytes;
    }
    
    /**
     * Set the frame buffer size.
     *
     * @return  void
     */
    void setFrameBufferSize(uint32_q frameBufferSize)
    {
      this->frameBufferSize = frameBufferSize;
    }
    
    /**
     * Reset the written bytes count.
     *
     * @return  void
     */
    void resetWrittenBytes()
    {
      writtenBytes = 0;
    }
    
    /**
     * Reset the read bytes count.
     *
     * @return  void
     */
    void resetReadBytes()
    {
      readBytes = 0;
    }
  };
}


#endif // Stream_H
