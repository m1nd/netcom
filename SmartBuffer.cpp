#include "SmartBuffer.h"


SmartBuffer::SmartBuffer()
{
  // Left empty
}

SmartBuffer::~SmartBuffer()
{
  // Left empty
}

SmartBuffer &SmartBuffer::operator<<(byte_q dataByte)
{
  buffer.push_back(dataByte);
  
  return *this;
}

SmartBuffer &SmartBuffer::operator<<(byte_q *dataBytes)
{
  // Add data array to the buffer
  for(auto i = 0; i < char_traits<byte_q>::length(dataBytes); i++)
    buffer.push_back(dataBytes[i]);
  
  return *this;
}

SmartBuffer &SmartBuffer::operator<<(string dataBytes)
{
  // Add data array to the buffer
  for(auto i = 0; i < dataBytes.length(); i++)
    buffer.push_back(dataBytes[i]);
  
  return *this;
}

void SmartBuffer::add(byte_q *dataBytes, uint64_q length)
{
  // Add data array to the buffer
  for(auto i = 0; i < length; i++)
    buffer.push_back(dataBytes[i]);
}

byte_q *SmartBuffer::data()
{
  return buffer.data();
}

void SmartBuffer::clear()
{
  buffer.clear();
}
