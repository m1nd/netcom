#ifndef Socket_H
#define Socket_H

//
// Includes
//
#include "Netcom.h"
#include "Endpoint.h"
#include "SocketException.h"


namespace Winux
{
  class Socket
  {
  protected:
    /**
     * Network interface
     */
    Network::Endpoint iface;
    
    
  public:
    /**
     * Default destructor
     */
    virtual ~Socket() {}
    
    /**
     * Perform an OS specific setup
     * and initialize the socket.
     *
     * @return  void
     */
    virtual void setup() = 0;
    
    /**
     * Bind the socket to a specific port.
     *
     * @param port  Port to bind
     * @return      void
     */
    void bind(uint32_q port)
    {
      iface.setPort(port);
      iface.getSocketAddress()->sin_addr.s_addr = INADDR_ANY;
      iface.bind();
    }
    
    /**
     * Close the socket.
     *
     * @return  void
     */
    void close()
    {
      iface.close();
    }
    
    /**
     * Set the SO_REUSEADDR flag.
     *
     * @param   state State
     * @return  void
     */
    void reuseAddress(bool state)
    {
      iface.reuseAddress(state);
    }
    
    /**
     * Set the SO_KEEPALIVE flag.
     *
     * @param   state State
     * @return  void
     */
    void keepAlive(bool state)
    {
      iface.keepAlive(state);
    }
    
    /**
     * Get the socket port.
     *
     * @return  Socket port
     */
    uint32_q getPort()
    {
      return iface.getPort();
    }
    
    /**
     * Get the socket IP address.
     *
     * @return  Socket IP address
     */
    string getIPAddress()
    {
      return iface.getIpAddress();
    }
  };
}


#endif // Socket_H
