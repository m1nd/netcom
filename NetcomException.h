#ifndef NetcomException_H
#define NetcomException_H


//
// Includes
//
#include "Netcom.h"
#include "Error.h"


class NetcomException
{
protected:
  /**
   * Exception message
   */
  string message;
  
  /**
   * Exception error code
   */
  int32_q error;
  
  
public:
  /**
   * Default constructor
   */
  NetcomException() : message("Exception message not set."), error(0) {}
  
  /**
   * Overloaded constructor
   */
  NetcomException(const string message, const int32_q error) : message(message), error(error) {}
  
  /**
   * Default destructor
   */
  virtual ~NetcomException() {}
  
  /**
   * Get the exception message.
   */
  string getMessage()
  {
    return message;
  }
  
  /**
   * Get the exception error code.
   */
  int32_q getError()
  {
    return error;
  }
};


#endif  // NetcomException_H
