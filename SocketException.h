#ifndef SocketException_H
#define SocketException_H

//
// Include
//
#include "NetcomException.h"


class SocketException : public NetcomException
{
public:
  /**
   * Overloaded constructor
   */
  SocketException(const string message, const int32_q error)
  {
    this->message = message;
    this->error = error;
  }
  
  /**
   * Default destructor
   */
  virtual ~SocketException() {}
};


#endif  // SocketException_H
