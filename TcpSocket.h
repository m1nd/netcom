#ifndef TcpSocket_H
#define TcpSocket_H

//
// Include
//
#include "Socket.h"


namespace Winux
{
  class TcpSocket : public Socket
  {
  private:
    /**
     * Copy constructor
     */
    TcpSocket(const TcpSocket &original);
    
    /**
     * Assignment operator
     */
    TcpSocket &operator=(const TcpSocket &source);
    
    
  public:
    /**
     * Default constructor
     */
    TcpSocket();
    
    /**
     * Overloaded constructor
     */
    TcpSocket(Network::Endpoint endpoint);
    
    /**
     * Default destructor
     */
    virtual ~TcpSocket();
    
    /**
     * Perform an OS specific setup
     * and initialize the socket.
     *
     * @return  void
     */
    void setup();
    
    /**
     * Set the socket into listen mode.
     *
     * @param maxPendingConnections Maximum pending connections
     * @return                      void
     */
    void listen(uint32_q maxPendingConnections);
    
    /**
     * Accept a client who connects to the socket.
     *
     * @return  TcpSocket object
     */
    TcpSocket *accept();
    
    /**
     * Connect to an endpoint via IP address / hostname and port.
     *
     * @param hostname  Target hostname or IP address
     * @param port      Target port
     * @return          void
     */
    void connect(string hostname, uint32_q port);
    
    /**
     * Send data to the socket.
     *
     * @param data    Data
     * @param length  How much data will be sent
     * @return        Sent bytes
     */
    uint64_q send(const byte_q *data, uint64_q length);
    
    /**
     * Receive data from the socket.
     *
     * @param buffer  Buffer receiving the data
     * @param length  How much data will be read
     * @return        Received bytes
     */
    uint64_q recv(byte_q *buffer, uint64_q length);
    
    /**
     * Check if the socket is open.
     *
     * @return  Socket is open (?)
     */
    bool isOpen()
    {
      return iface.getConnectionState();
    }
  };
}


#endif  // TcpSocket_H
