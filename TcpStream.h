#ifndef TcpStream_H
#define TcpStream_H

//
// Includes
//
#include "Stream.h"
#include "TcpSocket.h"


namespace Network
{
  class TcpStream : public Stream
  {
  private:
    /**
     * Pointer to the socket
     */
    Winux::TcpSocket *socket;
    
    
    /**
     * Copy constructor
     */
    TcpStream(TcpStream &original);
    
    /**
     * Assignment operator
     */
    TcpStream &operator=(const TcpStream &source);
    
    
  public:
    /**
     * Overloaded constructor
     */
    TcpStream(Winux::TcpSocket *socket);
    
    /**
     * Overloaded constructor
     */
    TcpStream(Winux::TcpSocket *socket, uint32_q frameBufferSize);
    
    /**
     * Default destructor
     */
    virtual ~TcpStream();
    
    /**
     * Write data to the stream.
     *
     * @param buffer  Buffer holding the data to write
     * @return        Object reference
     */
    TcpStream &operator<<(SmartBuffer &buffer);
    
    /**
     * Read data from the stream.
     *
     * @param buffer  Buffer receiving the data
     * @return        Object reference
     */
    TcpStream &operator>>(SmartBuffer &buffer);
  };
}


#endif  // TcpStream_H
