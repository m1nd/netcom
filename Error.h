#ifndef Error_H
#define Error_H


//
// Custom error codes
//

namespace Error
{
  class Endpoint
  {
  public:
    const static int NO_HOSTNAME = 100000;
    const static int RESOLVE_HOSTNAME_FAILED = 100001;
    const static int CONNECTION_CLOSED = 100002;
  };
}


#endif  // Error_H
