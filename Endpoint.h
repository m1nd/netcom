#ifndef Endpoint_H
#define Endpoint_H

//
// Includes
//
#include "Netcom.h"
#include "EndpointException.h"


namespace Network
{
  class Endpoint
  {
  private:
    /**
     * Generic socket error
     */
    const static uint64_q ENDPOINT_ERROR = -1;
    
    /**
     * Port
     */
    uint32_q port;
    
    /**
     * IP address
     */
    string ipAddress;
    
    /**
     * Connection state
     */
    bool connectionState;
    
    /**
     * Socket
     */
    sock_q socket;
    
    /**
     * Socket address
     */
    struct sockaddr_in address;
    
    
    /**
     * Resolve an IP address or a hostname.
     *
     * @throws          EndpointException
     * @param hostname  IP address or hostname
     * @return          void
     */
    void lookup(string hostname);
    
    /**
     * Get the error code.
     *
     * @return  Error code
     */
    int32_q getError();
    
    
  public:
    /**
     * Default constructor
     */
    Endpoint();
    
    /**
     * Default destructor
     */
    virtual ~Endpoint();
    
    /**
     * Copy constructor
     */
    Endpoint(Endpoint &original);
    
    /**
     * Assignment operator
     */
    Endpoint &operator=(Endpoint &source);
    
    /**
     * Set the port.
     *
     * @param port  Port
     * @return      void
     */
    void setPort(uint32_q port);
    
    /**
     * Set the host.
     *
     * @param hostname  IP address or hostname
     * @return          void
     */
    void setHost(string hostname);
    
    /**
     * Set the connection state.
     *
     * @param connectionState Connection state
     * @return                void
     */
    void setConnectionState(bool connectionState)
    {
      this->connectionState = connectionState;
    }
    
    /**
     * Set the socket.
     *
     * @throws        EndpointException
     * @param socket  Socket
     * @return        void
     */
    void setSocket(sock_q socket);
    
    /**
     * Set socket address.
     *
     * @param address Socket address
     * @return        void
     */
    void setSocketAddress(struct sockaddr_in address)
    {
      this->address = address;
    }
    
    /**
     * Get the port.
     *
     * @return  Port
     */
    uint32_q getPort()
    {
      return port;
    }
    
    /**
     * Get the IP address.
     *
     * @return  IP address
     */
    string getIpAddress()
    {
      return ipAddress;
    }
    
    /**
     * Get the connection state.
     *
     * @return  Connection state
     */
    bool getConnectionState()
    {
      return connectionState;
    }
    
    /**
     * Get endpoint's socket.
     *
     * @return  Socket
     */
    sock_q getSocket()
    {
      return socket;
    }
    
    /**
     * Get the socket address.
     *
     * @return  Socket address pointer
     */
    struct sockaddr_in *getSocketAddress()
    {
      return &address;
    }
    
    /**
     * Bind the socket.
     *
     * @throws      EndpointException
     * @return      void
     */
    void bind();
    
    /**
     * Set the socket into listen mode.
     *
     * @throws                      EndpointException
     * @param maxPendingConnections Maximum pending connections
     * @return                      void
     */
    void listen(uint32_q maxPendingConnections);
    
    /**
     * Accept a new endpoint.
     *
     * @throws  EndpointException
     * @return  Endpoint object pointer
     */
    Endpoint *accept();
    
    /**
     * Connect to the endpoint.
     *
     * @throws  EndpointException
     * @return  void
     */
    void connect();
    
    /**
     * Send data to the endpoint. (SOCK_STREAM)
     *
     * @throws        EndpointException
     * @param data    Data
     * @param length  How much data will be sent
     * @return        Sent bytes
     */
    uint64_q send(const byte_q *data, uint64_q length);
    
    /**
     * Receive data from the endpoint. (SOCK_STREAM)
     *
     * @throws        EndpointException
     * @param buffer  Buffer receiving the data
     * @param length  How much data will be read
     * @return        Received bytes
     */
    uint64_q recv(byte_q *buffer, uint64_q length);
    
    /**
     * Close the connection to the endpoint.
     *
     * @return void
     */
    void close();
    
    /**
     * Reuse the address. (Port number)
     *
     * @throws  EndpointException
     * @return  void
     */
    void reuseAddress(bool state);
    
    /**
     * Keep the connection alive. (Heartbeat)
     *
     * @throws  EndpointException
     * @return  void
     */
    void keepAlive(bool state);
  };
}


#endif  // Endpoint_H
