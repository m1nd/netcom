CPPFLAGS  = -Wall -g -std=c++11

OBJECTS = main.o Endpoint.o TcpSocket.o SmartBuffer.o TcpStream.o
BINARY = netcom

default:
	@echo "To build $(BINARY) enter: make TARGET"
	@echo
	@echo "Supported TARGETs:"
	@echo "[+] win: Windows"
	@echo "[+] linux: Linux"

linux: $(OBJECTS)
	g++ $(CPPFLAGS) -pthread -D_LINUX_OS_ -o $(BINARY) $(OBJECTS)

win: $(OBJECTS)
	g++ $(CPPFLAGS) -D_WIN_OS_ -o $(BINARY).exe $(OBJECTS)

.PHONY: clean
clean:
	rm -rf $(BINARY)* $(OBJECTS)
