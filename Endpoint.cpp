#include "Endpoint.h"

using namespace Network;


Endpoint::Endpoint() : port(0), ipAddress("0.0.0.0"), connectionState(false), socket(0)
{
  // Init endpointAddr
  memset(&address, '\0', sizeof(address));
  address.sin_family = AF_INET;
  
  // Format port to network byte order (big endian)
  address.sin_port = htons(port);
}

Endpoint::~Endpoint()
{
  // Left empty
}

Endpoint::Endpoint(Endpoint &original)
{
  // Copy attributes
  this->port = original.getPort();
  this->ipAddress = original.getIpAddress();
  this->connectionState = original.getConnectionState();
  this->socket = original.getSocket();
  this->address = *original.getSocketAddress();
}

Endpoint &Endpoint::operator=(Endpoint &source)
{
  // Parse attributes
  this->port = source.getPort();
  this->ipAddress = source.getIpAddress();
  this->connectionState = source.getConnectionState();
  this->socket = source.getSocket();
  this->address = *source.getSocketAddress();
  
  return *this;
}

void Endpoint::setPort(uint32_q port)
{
  // Format port to network byte order (big endian)
  address.sin_port = htons(port);
  
  this->port = port;
}

void Endpoint::setHost(string hostname)
{
  // Get host by hostname / IP address (trivial)
  lookup(hostname);
  
  // inet_ntoa --> Human readable IP address format
  this->ipAddress = inet_ntoa(address.sin_addr);
}

void Endpoint::lookup(string hostname)
{
  if(hostname.empty())
    throw EndpointException("No hostname.", Error::Endpoint::NO_HOSTNAME);
  
  // Check if the hostname is an IP address
  // hostname: (std::string) --> &hostname[0]: (char *)
  uint32_q ip = inet_addr(&hostname[0]);
  
  if(ip != INADDR_NONE)
  {
    address.sin_addr.s_addr = ip;
    return;
  }
  
  // Check if the hostname is a hostname
  // hostname: (std::string) --> &hostname[0]: (char *)
  struct hostent *hostent = gethostbyname(&hostname[0]);
  
  if(hostent == NULL)
    throw EndpointException("Resolving hostname failed.", Error::Endpoint::RESOLVE_HOSTNAME_FAILED);
  
  // Memcpy IP address (4 bytes) from hostent to addr struct
  memcpy(&(address.sin_addr), hostent->h_addr_list[0], hostent->h_length);
}

void Endpoint::setSocket(sock_q socket)
{
  this->socket = socket;
  
  // This exception should be catched and re-thrown with
  // an accurate error message in all upper level classes
  // which implement this function
  if(this->socket == static_cast<uint32_q>(ENDPOINT_ERROR))
    throw EndpointException("Generic socket error.", getError());
}

void Endpoint::bind()
{
  // Bind socket
  if(::bind(socket, (struct sockaddr *) &address, sizeof(struct sockaddr)) == ENDPOINT_ERROR)
    throw EndpointException("Binding socket failed.", getError());
  
  this->connectionState = true;
}

void Endpoint::listen(uint32_q maxPendingConnections)
{
  // Set socket into listen mode
  if(::listen(socket, maxPendingConnections) == ENDPOINT_ERROR)
    throw EndpointException("Setting socket into listen mode failed.", getError());
}

Endpoint *Endpoint::accept()
{
  Endpoint *endpoint = new Endpoint();
  
#ifdef _WIN_OS_
  // Windows wants an int
  int32_q addressLength = sizeof(struct sockaddr);
#else
  // Linux wants an unsigned int
  uint32_q addressLength = sizeof(struct sockaddr);
#endif
  
  try
  {
    // Accept new endpoint
    endpoint->setSocket(::accept(socket, (struct sockaddr *) endpoint->getSocketAddress(), &addressLength));
  }
  catch(EndpointException &e)
  {
    throw EndpointException("Accepting new endpoint failed.", e.getError());
  }
  
  // Retrieve information about the entpoint
  if(getpeername(endpoint->getSocket(), (struct sockaddr *) endpoint->getSocketAddress(), &addressLength) == ENDPOINT_ERROR)
    throw EndpointException("Getting endpoint's peer name failed.", getError());
  
  // Get endpoint's port and IP address
  endpoint->setPort(ntohs(endpoint->getSocketAddress()->sin_port));
  endpoint->setHost(inet_ntoa(endpoint->getSocketAddress()->sin_addr));
  endpoint->setConnectionState(true);
  
  return endpoint;
}

void Endpoint::connect()
{
  // Connect to the socket
  if(::connect(socket, (struct sockaddr *) &address, sizeof(struct sockaddr)) == ENDPOINT_ERROR)
    throw EndpointException("Connection refused.", getError());
  
  this->connectionState = true;
}

uint64_q Endpoint::send(const byte_q *data, uint64_q length)
{
  uint64_q sentBytes = ::send(socket, data, length, 0);
  
  // Error check
  if(sentBytes == ENDPOINT_ERROR)
  {
#ifdef _WIN_OS_
    if(getError() == WSAECONNRESET)
      throw EndpointException("Sending aborted. Connection closed by peer.", getError());
    else
      throw EndpointException("Sending failed.", getError());
#else // -- Linux --
    if(getError() == ECONNRESET)
      throw EndpointException("Sending aborted. Connection closed by peer.", getError());
    else
      throw EndpointException("Sending failed.", getError());
#endif
  }
  
  return sentBytes;
}

uint64_q Endpoint::recv(byte_q *buffer, uint64_q length)
{
  uint64_q recvBytes = ::recv(socket, buffer, length, 0);
  
  // Error check
  if(recvBytes == ENDPOINT_ERROR)
  {
#ifdef _WIN_OS_
    if(getError() == WSAECONNRESET)
      recvBytes = 0;
    else
      throw EndpointException("Receiving failed.", getError());
#else // -- Linux --
    if(getError() == ECONNRESET)
      recvBytes = 0;
    else
      throw EndpointException("Receiving failed.", getError());
#endif
  }
  
  return recvBytes;
}

void Endpoint::close()
{
  if(connectionState == true)
  {
#ifdef _WIN_OS_
    closesocket(socket);
#else// -- Linux --
    ::close(socket);
#endif
  }
  
#ifdef _WIN_OS_
  // Windows specific cleanup
  WSACleanup();
#endif
  
  this->connectionState = false;
}

void Endpoint::reuseAddress(bool state)
{
#ifdef _WIN_OS_
  char value = static_cast<char>(state);
#else
  int value = static_cast<int>(state);
#endif
  
  // Set SO_REUSEADDR socket option
  if(setsockopt(socket, SOL_SOCKET, SO_REUSEADDR, &value, sizeof(value)) == ENDPOINT_ERROR)
    throw EndpointException("Setting SO_REUSEADDR option failed.", getError());
}

void Endpoint::keepAlive(bool state)
{
#ifdef _WIN_OS_
  char value = static_cast<char>(state);
#else
  int value = static_cast<int>(state);
#endif
  
  // Set SO_KEEPALIVE socket option
  if(setsockopt(socket, SOL_SOCKET, SO_KEEPALIVE, &value, sizeof(value)) == ENDPOINT_ERROR)
    throw EndpointException("Setting SO_KEEPALIVE option failed.", getError());
}

int32_q Endpoint::getError()
{
#ifdef _WIN_OS_
  return WSAGetLastError();
#else
  return errno;
#endif
}
