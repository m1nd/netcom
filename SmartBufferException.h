#ifndef SmartBufferException_H
#define SmartBufferException_H

//
// Include
//
#include "NetcomException.h"


class SmartBufferException : public NetcomException
{
public:
  /**
   * Overloaded constructor
   */
  SmartBufferException(const string message, const int32_q error)
  {
    this->message = message;
    this->error = error;
  }

  /**
   * Default destructor
   */
  virtual ~SmartBufferException() {}
};


#endif  // SmartBufferException_H
