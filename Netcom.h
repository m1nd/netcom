#ifndef Netcom_H
#define Netcom_H


//
// -> DEBUGGING
//
// #define _WIN_OS_
//
// DEBUGGING <-
//


//
// Common includes
//
#include <string.h> // memset, memcpy, ..
#include <string>   // std::string
#include <vector>   // std::vector


//
// OS targeting includes
//
#ifdef _WIN_OS_
#include <WinSock2.h>               // Socket
#pragma comment(lib, "Ws2_32.lib")  // Require library
#else // -- Linux --
#include <sys/socket.h> // Socket
#include <netinet/in.h> // sockaddr_in struct
#include <arpa/inet.h>  // inet_addr struct
#include <netdb.h>      // hostent struct
#include <errno.h>      // Error codes
#include <unistd.h>     // close()
#endif


//
// API includes
//
#include "Types.h"  // Datatypes


//
// Usings
//
using std::string;
using std::vector;
using std::char_traits;


#endif
