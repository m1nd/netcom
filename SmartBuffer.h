#ifndef SmartBuffer_H
#define SmartBuffer_H

//
// Includes
//
#include "Netcom.h"
#include "SmartBufferException.h"


class SmartBuffer
{
private:
  /**
   * Dynamic array
   */
  vector<byte_q> buffer;
  
  
  /**
   * Copy constructor
   */
  SmartBuffer(SmartBuffer &original);
  
  /**
   * Assignment operator
   */
  SmartBuffer &operator=(const SmartBuffer &source);
  
  
public:
  /**
   * Default constructor
   */
  SmartBuffer();
  
  /**
   * Default destructor
   */
  virtual ~SmartBuffer();
  
  /**
   * Add one data byte to the buffer.
   *
   * @param dataByte  Data byte
   * @return          Object reference
   */
  SmartBuffer &operator<<(byte_q dataByte);
  
  /**
   * Add a data byte array to the buffer.
   *
   * @param dataBytes Data byte array
   * @return          Object reference
   */
  SmartBuffer &operator<<(byte_q *dataBytes);
  
  /**
   * Add a data byte array to the buffer.
   *
   * @param dataBytes Data byte string
   * @return          Object reference
   */
  SmartBuffer &operator<<(string dataBytes);
  
  /**
   * Add a data byte array with a certain
   * length to the buffer.
   *
   * @param dataBytes Data byte array
   * @param length    How much data will be added
   * @return          void
   */
  void add(byte_q *dataBytes, uint64_q length);
  
  /**
   * Get a pointer to the data.
   *
   * @return  Pointer to the data bytes
   */
  byte_q *data();
  
  /**
   * Clear the buffer.
   *
   * @return  void
   */
  void clear();
  
  /**
   * Get the buffer size.
   *
   * @return  void
   */
  uint64_q size()
  {
    return buffer.size();
  }
};


#endif  // SmartBuffer_H
