#include "TcpSocket.h"

using namespace Winux;


TcpSocket::TcpSocket()
{
  // Left empty
}

TcpSocket::TcpSocket(Network::Endpoint endpoint)
{
  this->iface = endpoint;
}

TcpSocket::~TcpSocket()
{
  // Left empty
}

void TcpSocket::setup()
{
#ifdef _WIN_OS_
  WSADATA wsaData;  // Windows specific initialization
  if(WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) // WinSock v2.2
    throw SocketException("Initializing winsock failed.", WSAGetLastError());
#endif
  
  try
  {
    // Create a TCP socket
    // AF_INET:     IPv4
    // SOCK_STREAM: 2-Way communication via TCP and IPv4
    // IPPROTO_TCP: TCP protocol
    iface.setSocket(socket(AF_INET, SOCK_STREAM, IPPROTO_TCP));
  }
  catch(EndpointException &e)
  {
    throw SocketException("Creating TCP socket failed.", e.getError());
  }
}

void TcpSocket::listen(uint32_q maxPendingConnections)
{
  iface.listen(maxPendingConnections);
}

TcpSocket *TcpSocket::accept()
{
  // Create a new tcp socket based
  // on the newly accepted entpoint
  Network::Endpoint *endpoint = iface.accept();
  TcpSocket *tcpSocket = new TcpSocket(*endpoint);
  
  // Cleanup
  delete endpoint;
  
  return tcpSocket;
}

void TcpSocket::connect(string hostname, uint32_q port)
{
  iface.setPort(port);
  iface.setHost(hostname);
  iface.connect();
}

uint64_q TcpSocket::send(const byte_q *data, uint64_q length)
{
  return iface.send(data, length);
}

uint64_q TcpSocket::recv(byte_q *buffer, uint64_q length)
{
  return iface.recv(buffer, length);
}
