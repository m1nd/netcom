#include "TcpStream.h"

using namespace Network;


TcpStream::TcpStream(Winux::TcpSocket *socket) : socket(socket)
{
  // Left empty
}

TcpStream::TcpStream(Winux::TcpSocket *socket, uint32_q frameBufferSize) : socket(socket)
{
  this->frameBufferSize = frameBufferSize;
}

TcpStream::~TcpStream()
{
  // Left empty
}

TcpStream &TcpStream::operator<<(SmartBuffer &buffer)
{
  uint32_q offset = 0;
  byte_q *data = buffer.data();
  
  // Send full data frames
  while((buffer.size() - offset) >= frameBufferSize)
  {
    writtenBytes += socket->send(data, frameBufferSize);
    offset += frameBufferSize;
    
    // Increase pointer value
    // |x|.|.|.|y|.|  -->  |x|.|.|.|y|.|
    //  ^                           ^
    //  |                           |
    data += offset;
  }
  
  // Send the remaining data frame (smaller than frameBufferSize)
  writtenBytes += socket->send(data, buffer.size() % frameBufferSize);
  
  return *this;
}

TcpStream &TcpStream::operator>>(SmartBuffer &buffer)
{
  // Allocate some space
  byte_q *tmpBuffer = new byte_q[frameBufferSize];
  
  // Read data
  uint64_q read = socket->recv(tmpBuffer, frameBufferSize);
  
  // Check if the socket has been closed by the peer
  if(read == 0)
  {
    // Close the socket too
    socket->close();
    
    // Cleanup
    delete [] tmpBuffer;
    return *this;
  }
  
  // Add the data to the buffer
  buffer.add(tmpBuffer, read);
  readBytes += read;
  
  // Cleanup
  delete [] tmpBuffer;
  
  return *this;
}
