#ifndef EndpointException_H
#define EndpointException_H

//
// Include
//
#include "NetcomException.h"


class EndpointException : public NetcomException
{
public:
  /**
   * Overloaded constructor
   */
  EndpointException(const string message, const int32_q error)
  {
    this->message = message;
    this->error = error;
  }
  
  /**
   * Default destructor
   */
  virtual ~EndpointException() {}
};


#endif  // EndpointException_H
